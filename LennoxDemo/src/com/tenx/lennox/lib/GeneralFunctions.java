package com.tenx.lennox.lib;

import java.io.File;
import java.io.FileInputStream;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.sun.org.apache.bcel.internal.generic.Select;

import jxl.Sheet;
import jxl.Workbook;

public class GeneralFunctions {
	
	public static int getExcelRowCount(String sFilePath, String sSheetName) {  
		
		int rowCount = 0;
		
		try{
			
			Workbook workbook = Workbook.getWorkbook(new FileInputStream(new File(sFilePath)));
			Sheet sheet = workbook.getSheet(sSheetName);
			
			rowCount = sheet.getRows();
			
			return rowCount;
			
		} 
		
		
		catch(Exception e) {
			e.printStackTrace();
			return rowCount;
		}
		
	}
	
//	public static boolean SelectByVisibleText(WebDriver driver, By locator,String sText){
//		try{
//			
//			WebElement element=driver.findElement(locator);
//			Select se=new Select(element);
//			se.selectByVisibleText(sText);
//		}catch(Exception e){
//			e.printStackTrace();
//			return false;
//		}
//		return true;
//		
//	}

}
