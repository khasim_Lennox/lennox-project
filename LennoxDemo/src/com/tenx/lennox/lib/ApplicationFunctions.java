package com.tenx.lennox.lib;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.collections.iterators.EntrySetMapIterator;
import org.jboss.netty.channel.local.LocalAddress;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.tenx.framework.lib.Alerts;
import com.tenx.framework.lib.Browser;
import com.tenx.framework.lib.Elements;
import com.tenx.framework.lib.Messages;
import com.tenx.framework.lib.Utilities;
import com.tenx.framework.lib.Wait;
import com.tenx.lennox.locators.Locators;
import com.tenx.lennox.variables.Constants;
import com.tenx.lennox.variables.Global;

public class ApplicationFunctions {

	private static boolean bStatus;
	private static Map <String , Map <String, String>> AllProductDetails = new HashMap<String , Map <String, String>>();
	private static List <String> productIdList = new ArrayList<String>();
	private static List <String> quickOrderProductIDList = new ArrayList<String>(); 

	private static String sOrderNumber;
	private static String sEstimatedArrival;

	public static boolean loginToApplication(String sUserId, String sPassword) {

		try{

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Link.signIn, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Home page not displayed";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Link.signIn);
			if(!bStatus) {
				Messages.errorMsg = "Login link not clicked";
				return false;
			}

			Thread.sleep(3000);

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.memberSignIn, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Sign in button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.memberSignIn);
			if(!bStatus) {
				Messages.errorMsg = "Sign in button not clicked";
				return false;
			}

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.LoginPage.TextBox.userId, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Login page not displayed";
				return false;
			}

			bStatus = Elements.enterText(Global.wDriver, Locators.LoginPage.TextBox.userId, sUserId);
			if(!bStatus) {
				Messages.errorMsg = "UserId not entered";
				return false;
			}

			bStatus = Elements.enterText(Global.wDriver, Locators.LoginPage.TextBox.password, sPassword);
			if(!bStatus) {
				Messages.errorMsg = "Password not entered";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.LoginPage.Button.signIn);
			if(!bStatus) {
				Messages.errorMsg = "Sign in button not clicked";
				return false;
			}

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.TextField.welcomeMessage, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Login page not displayed";
				return false;
			}

			String message = Elements.getText(Global.wDriver, Locators.HomePage.TextField.welcomeMessage);
			//if (!message.contains("Welcome,"+Global.sUserName))
			if(!message.contains("Welcome,")){

				Messages.errorMsg = "Invalid Welcome user message";
				return false;
			}

			return true;

		} catch (Exception e) {

			e.printStackTrace();
			return false;

		}
	}

	public static boolean clearCart() {

		try{

			String cartQuantity = Elements.getText(Global.wDriver, Locators.HomePage.Button.viewCart);
			if(!cartQuantity.contains("(0)")) {

				bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.viewCart, Constants.waitForPage);
				if(!bStatus) {
					Messages.errorMsg = "View Cart button not visible";
					return false;
				}

				bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.viewCart);
				if(!bStatus) {
					Messages.errorMsg = "View Cart button not clicked";
					return false;
				}

				bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Link.viewItemsInCart, Constants.waitForPage);
				if(!bStatus) {
					Messages.errorMsg = "View all items link not visible";
					return false;
				}

				bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Link.viewItemsInCart);
				if(!bStatus) {
					Messages.errorMsg = "View all items link not clicked";
					return false;
				}

				bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.CartPage.Link.clearCart, Constants.waitForPage);
				if(!bStatus) {
					Messages.errorMsg = "Clear cart link not visible";
					return false;
				}

				bStatus = Elements.clickButton(Global.wDriver, Locators.CartPage.Link.clearCart);
				if(!bStatus) {
					Messages.errorMsg = "Clear cart link not clicked";
					return false;
				}

				/*bStatus = Alerts.acceptAlert(Global.wDriver);
				if(!bStatus) {
					Messages.errorMsg = "Alert not accepted";
					return false;
				}
				
				*/
				    Thread.sleep(3000);
				   bStatus=Wait.waitForElementVisibility(Global.wDriver,Locators.CartPage.Button.areYouSureYesButton,Constants.waitForPage);
						if(!bStatus)
						{
							Messages.errorMsg = "Yes button not visible";
							return false;
						}
				
						
						bStatus=Elements.clickButton(Global.wDriver,Locators.CartPage.Button.areYouSureYesButton);
						if(!bStatus)
						{
							Messages.errorMsg = "Yes button not clickable";
							return false;
						}
				
				bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.logo, Constants.waitForPage);
				if(!bStatus) {
					Messages.errorMsg = "Home button not visible";
					return false;
				}

				bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.logo);
				if(!bStatus) {
					Messages.errorMsg = "Home button not clicked";
					return false;
				}

				bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.viewCart, Constants.waitForPage);
				if(!bStatus) {
					Messages.errorMsg = "View Cart button not visible";
					return false;
				}

				cartQuantity = Elements.getText(Global.wDriver, Locators.HomePage.Button.viewCart);
				if(!cartQuantity.contains("(0)")) {
					Messages.errorMsg = "Cart not cleared";
					return false;
				}

			}

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static boolean navigateToHomePage() {
		try {
			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.logo, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Home button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.logo);
			if(!bStatus) {
				Messages.errorMsg = "Home button not clicked";
				return false;
			}

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean selectProductCatalogType(Map<String, String> details){
		try {

			String sCatalogTypeLocator = Locators.HomePage.Link.productLink.replace("prodCatType", details.get("Product Catalog")); 

			bStatus = Wait.waitForElementVisibility(Global.wDriver, By.xpath(sCatalogTypeLocator), Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Product catalog type not visible.";
			}

			bStatus = Elements.clickButton(Global.wDriver, By.xpath(sCatalogTypeLocator));
			if(!bStatus){
				Messages.errorMsg = "Could not select product catalog type.";
				return false;
			}

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static boolean selectSubCategory(Map<String, String> details) {

		try{

			String menuPath = details.get("MenuPath");

			String[] subCategories = menuPath.split("/");

			for(String category : subCategories) {

				String sCategoryLocator = Locators.CatalogPage.Link.category.replace("subCategory", category); 

				bStatus = Wait.waitForElementVisibility(Global.wDriver, By.xpath(sCategoryLocator), Constants.waitForElement);
				if(!bStatus) {
					Messages.errorMsg = "Product sub category "+category+"not visible";
				}

				bStatus = Elements.clickButton(Global.wDriver, By.xpath(sCategoryLocator));
				if(!bStatus){
					Messages.errorMsg = "Could not select product sub category "+category;
					return false;
				}

			}

			return true;

		} catch (Exception e) {

			e.printStackTrace();
			return false;

		}

	}

	public static boolean selectAProduct(Map<String, String> details) {

		try{

			productIdList.add(details.get("ProductId"));

			Map<String,String> productDetails = new HashMap<String, String>();

			String sProductIdLocator = Locators.CatalogPage.TextField.productId.replace("productId", details.get("ProductId"));
			bStatus = Wait.waitForElementVisibility(Global.wDriver, By.xpath(sProductIdLocator), Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Product not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, By.xpath(sProductIdLocator));
			if(!bStatus) {
				Messages.errorMsg = "Product not selected";
				return false;
			}
			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.CatalogPage.TextField.description, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Product not Opened successfully";
				return false;
			}
			return true;

		} catch (Exception e) {

			e.printStackTrace();
			return false;

		}

	}
	public static boolean readSpecification(Map<String,String> specificationDetails, int iRowCounter){
		try{
			Map<String , String> ProductSpecfications= new LinkedHashMap<String, String>();
			List<String> listKey= new LinkedList<String>();

			String sProductId = specificationDetails.get("ProductId");
			listKey.add("ProductId");
			ProductSpecfications.put("ProductId", sProductId);

			//			String sYourPrice = AllProductDetails.get(sProductId).get("yourPrice");
			//			String sListPrice = AllProductDetails.get(sProductId).get("listPrice");
			//			listKey.add("YourPrice");
			//			ProductSpecfications.put("YourPrice", sYourPrice);
			//			listKey.add("ListPrice");
			//			ProductSpecfications.put("ListPrice", sListPrice);



			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.CatalogPage.Button.specifications, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Specification Button not visible";
				return false;
			}


			bStatus = Elements.clickButton(Global.wDriver, Locators.CatalogPage.Button.specifications);
			if(!bStatus) {
				Messages.errorMsg = "Specification Buton not clicked";
				return false;
			}


			bStatus = Wait.waitForElementVisibility(Global.wDriver, By.xpath("//div[@class='content tab-content trans active']/div/div"), Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Specification's Table is not displayed";
				return false;
			}
			int iSpecificationTableCount=Elements.getXpathCount(Global.wDriver, By.xpath("//div[@class='content tab-content trans active']/div/div"));
			for(int iCount=1;iCount<=iSpecificationTableCount;iCount++){
				int iDescriptionCount=Elements.getXpathCount(Global.wDriver,By.xpath("//div[@class='content tab-content trans active']/div/div["+iCount+"]/table/tbody/tr"));
				for(int iDetail=1;iDetail<=iDescriptionCount;iDetail++){
					String sKey=Elements.getText(Global.wDriver, By.xpath("//div[@class='content tab-content trans active']/div/div["+iCount+"]/table/tbody/tr["+iDetail+"]/td[1]"));
					listKey.add(sKey);
					String sValue=Elements.getText(Global.wDriver, By.xpath("//div[@class='content tab-content trans active']/div/div["+iCount+"]/table/tbody/tr["+iDetail+"]/td[2]"));
					ProductSpecfications.put(sKey, sValue);

				}	
			}
			//			
			//			Iterator itr= ProductSpecfications.entrySet().iterator();
			//			while(itr.hasNext()){
			//				 Map.Entry product = (Map.Entry)itr.next();
			//				 
			//				System.out.println(product.getKey());
			//				System.out.println(product.getValue());
			//			}



			ProductSpecfications.putAll(ApplicationFunctions.getProductPrice());
			listKey.addAll(ApplicationFunctions.getProductPrice().keySet());

			ProductSpecfications.putAll(ApplicationFunctions.productAvailability());

			listKey.addAll(ApplicationFunctions.productAvailability().keySet());

			Iterator itr= ProductSpecfications.entrySet().iterator();
			while(itr.hasNext()){
				Map.Entry product = (Map.Entry)itr.next();

				System.out.println(product.getKey());
				System.out.println(product.getValue());
			}
			bStatus=ApplicationFunctions.writeToResult(Global.sResultfilePath, ProductSpecfications,listKey,iRowCounter);
			if(!bStatus){
				return false;
			}

			Map<String,String> productDetails = new HashMap<>();

			productDetails.put("yourPrice", ApplicationFunctions.getProductPrice().get("YourPrice"));
			productDetails.put("listPrice", ApplicationFunctions.getProductPrice().get("ListPrice"));
			productDetails.put("quantity", specificationDetails.get("Quantity"));

			AllProductDetails.put(sProductId, productDetails);

			bStatus=Elements.enterText(Global.wDriver, Locators.CatalogPage.TextBox.quantity, specificationDetails.get("Quantity"));
			if(!bStatus){
				Messages.errorMsg = "Quantity not entered";
				return false;
			}

			String addToCartLocator = Locators.CatalogPage.Button.addToCart.replace("productId", specificationDetails.get("ProductId"));

			bStatus=Elements.clickButton(Global.wDriver, By.xpath(addToCartLocator));
			if(!bStatus){
				Messages.errorMsg = "Add to cart button not clicked";
				return false;
			}

			Browser.reloadPage(Global.wDriver);

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static boolean writeToResult(String sPathOfthefile,Map<String, String> SpecificDetails,List<String> listKey, int iRowCounter){
		try{
			Workbook wb = Workbook.getWorkbook(new File(sPathOfthefile));
			Sheet s = wb.getSheet("Result");

			int rowCount = s.getRows();

			List<String> headerKeyList = new ArrayList<>();

			int iHeadingColCount = s.getColumns();
			for(int i = 0; i<iHeadingColCount; i++) {
				Cell columnLabel = s.getCell(i, 0);
				headerKeyList.add(columnLabel.getContents());
			}

			boolean newKey = true;

			for(String key : listKey) {

				newKey = true;

				for(String existingHeader : headerKeyList) {

					if(key.equals(existingHeader)) {
						newKey = false;
						break;
					}

				}

				if(newKey == true) {
					headerKeyList.add(key);
				}

			}

			WritableWorkbook wbCopy = Workbook.createWorkbook(new File(sPathOfthefile), wb);
			WritableSheet sResult = wbCopy.getSheet("Result");

			int iCount = 0;

			for(String key : headerKeyList) {

				Label Rowlabel= new Label(iCount,0,key); 
				sResult.addCell(Rowlabel);
				iCount++;	
			}

			int iSpec_Count= SpecificDetails.size();
			for(iCount=0;iCount<headerKeyList.size();iCount++){

				String sRow = SpecificDetails.get(headerKeyList.get(iCount));

				if(sRow == null) {
					continue;
				}

				Label Rowlabel= new Label(iCount,rowCount+iRowCounter,sRow); 
				sResult.addCell(Rowlabel);

			}
			wbCopy.write();
			wbCopy.close();
			wb.close();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean addAProductusingQuickOrder(String productId, String quantity){
		try{

			productIdList.add(productId);
			quickOrderProductIDList.add(productId);

			Map <String,String> productDetails = new HashMap<String, String>();
			productDetails.put("quantity", quantity);

			AllProductDetails.put(productId, productDetails);

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.quickOrder, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Quick order button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.quickOrder);
			if(!bStatus) {
				Messages.errorMsg = "Quick order button not clicked";
				return false;
			}

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.TextBox.catalogNumberForQuickOrder, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Catalog number textbox not visible";
				return false;
			}

			bStatus = Elements.enterText(Global.wDriver, Locators.HomePage.TextBox.catalogNumberForQuickOrder, productId);
			if(!bStatus) {
				Messages.errorMsg = "Text not entered in Catalog number textbox field";
				return false;
			}

			bStatus = Elements.enterText(Global.wDriver, Locators.HomePage.TextBox.quantityForQuickOrder, quantity);
			if(!bStatus) {
				Messages.errorMsg = "Text not entered in Quantity textbox field";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.addToCartByQuickOrder);
			if(!bStatus) {
				Messages.errorMsg = "AddToCart button not clicked";
				return false;
			}

			Browser.reloadPage(Global.wDriver);

			return true;

		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean verifyCartMenu(String filePath, String sheetName){
		try{

			int index = 0;

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.viewCart, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "View Cart button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.viewCart);
			if(!bStatus) {
				Messages.errorMsg = "View Cart button not clicked";
				return false;
			}

//			float fExpSubTotal = 0;
//
//			for(String productId : quickOrderProductIDList) {
//
//				index++;
//
//				Map<String,String> productDetails = AllProductDetails.get(productId);
//
//				String sProductPriceLocator = Locators.HomePage.TextField.productPrice.replace("productId", productId);
//
//				bStatus = Wait.waitForElementVisibility(Global.wDriver, By.xpath(sProductPriceLocator), Constants.waitForElement);
//				if(!bStatus) {
//					Messages.errorMsg = "Product unit price is not visible";
//					return false;
//				}
//
//				String sYourPrice = Elements.getText(Global.wDriver, By.xpath(sProductPriceLocator));
//				sYourPrice = sYourPrice.substring(sYourPrice.indexOf('$')+1);
//				if(sYourPrice.length()>=8) {
//					sYourPrice = sYourPrice.replace(",", "");
//				}
//				float  yourPrice = Float.parseFloat(sYourPrice);
//				int quantity = Integer.parseInt(productDetails.get("quantity"));
//
//				productDetails.put("yourPrice", sYourPrice);
//				productDetails.put("listPrice", "NA");
//				AllProductDetails.put(productId, productDetails);
//
//				ApplicationFunctions.addQuickOrderProductDetailsToResultXls(filePath, sheetName, productId, String.valueOf(quantity), index);
//
//				fExpSubTotal += yourPrice*quantity;
//
//			}

			return true;

		} catch(Exception ex){

			ex.printStackTrace();
			return false;

		}

	}

	public static boolean verifyCartPage(Map<String, String> details){
		try{

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.checkout, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Checkout Button not visible";
				return false;
			}

			bStatus=Elements.clickButton(Global.wDriver, Locators.HomePage.Button.checkout);
			if(!bStatus){
				Messages.errorMsg="Checkout Button not clicked";
				return false;
			}



			float fExpWeight = 0;
			float fExpSubTotal = 0;

			for(String productId : productIdList) {

				Map<String,String> productDetails = new HashMap<String, String>();
				productDetails = AllProductDetails.get(productId);


				String sProductWeightLocator = Locators.CartPage.TextField.productWeight.replace("productId", productId);
				String sProductTotalPriceLocator = Locators.CartPage.TextField.totalPrice.replace("productId", productId);
				String sQuantity = AllProductDetails.get(productId).get("quantity");
				int quantity = Integer.parseInt(sQuantity);

				bStatus=Wait.waitForElementVisibility(Global.wDriver, By.xpath(sProductWeightLocator), Constants.waitForPage);
				if(!bStatus) {
					Messages.errorMsg = "Cart page not displayed";
					return false;
				}

				String sWeight = Elements.getText(Global.wDriver, By.xpath(sProductWeightLocator)).substring(8);
				sWeight = sWeight.substring(0, sWeight.indexOf(" ")); 
				productDetails.put("weight", sWeight);
				AllProductDetails.put(productId, productDetails);
				float fWeight = Float.parseFloat(sWeight);
				fExpWeight += fWeight*quantity;

				String sTotalPrice = Elements.getText(Global.wDriver, By.xpath(sProductTotalPriceLocator)).substring(1);
				if(sTotalPrice.length()>=8) {
					sTotalPrice = sTotalPrice.replace(",", "");
				}
				float ftotalprice = Float.parseFloat(sTotalPrice);
				fExpSubTotal += ftotalprice;

			}

			String sSubTotal = Elements.getText(Global.wDriver, Locators.CartPage.TextField.totalSubtotal);
			sSubTotal = sSubTotal.trim().substring(1);
			if(sSubTotal.length()>=8) {
				sSubTotal = sSubTotal.replace(",", "");
			}
			float fActualSubTotal = Float.parseFloat(sSubTotal);

			Assert.assertEquals(fActualSubTotal, fExpSubTotal);

			String sWeight = Elements.getText(Global.wDriver, Locators.CartPage.TextField.totalWeight);
			sWeight = sWeight.trim().substring(0, sWeight.indexOf(" "));
			float fActualWeight = Float.parseFloat(sWeight);

			Assert.assertEquals(fActualWeight, fExpWeight);

			String sEstimatedShipping = Elements.getText(Global.wDriver, Locators.CartPage.TextField.estimatedShipping);
			sEstimatedShipping = sEstimatedShipping.trim().substring(1);
			if(sEstimatedShipping.length()>=8) {
				sEstimatedShipping = sEstimatedShipping.replace(",", "");
			}
			float fEstimatedShipping = Float.parseFloat(sEstimatedShipping);

			String sEstimatedTaxes = Elements.getText(Global.wDriver, Locators.CartPage.TextField.estimatedTaxes);
			sEstimatedTaxes = sEstimatedTaxes.trim().substring(1);
			if(sEstimatedTaxes.length()>=8) {
				sEstimatedTaxes = sEstimatedTaxes.replace(",", "");
			}
			float fEstimatedTaxes = Float.parseFloat(sEstimatedTaxes);

			float fExpEstimatedTotal = fExpSubTotal + fEstimatedShipping + fEstimatedTaxes;

			String sEstimatedTotal = Elements.getText(Global.wDriver, Locators.CartPage.TextField.estimatedTotal);
			sEstimatedTotal = sEstimatedTotal.trim().substring(1);
			if(sEstimatedTotal.length()>=8) {
				sEstimatedTotal = sEstimatedTotal.replace(",", "");
			}
			float fActualEstimatedTotal = Float.parseFloat(sEstimatedTotal);

			Assert.assertEquals(fActualEstimatedTotal, fExpEstimatedTotal);

			/////////////////////////////////////////////////////////////////////////////////////////////////


			bStatus = Wait.waitForElementVisibility(Global.wDriver,Locators.AddToCartPage.Link.updateShopping, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Update Shipping/Pick link not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.AddToCartPage.Link.updateShopping );
			if(!bStatus) {
				Messages.errorMsg = "Update Shipping/Pick not clicked";
				return false;
			}
  
			
			Thread.sleep(3000);
			
			ApplicationFunctions.clickElementUsingJs(By.xpath(Locators.AddToCartPage.Dropdown.shippingMethod));
			
			
//			bStatus = Wait.waitForElementVisibility(Global.wDriver,By.xpath(Locators.AddToCartPage.Dropdown.shippingMethod), Constants.waitForElement);
//			if(!bStatus) {
//				Messages.errorMsg = "Shipping method field not visible";
//				return false;
//			}
           
			bStatus = Elements.selectOptionByVisibleText(Global.wDriver, Locators.AddToCartPage.Dropdown.shippingMethod, details.get("Shipping Method"));
			if(!bStatus) {
				Messages.errorMsg = "Shipping method not selected";
				return false;
			}


			bStatus = Wait.waitForElementVisibility(Global.wDriver,Locators.AddToCartPage.Button.updateAll, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "UpdateAll Button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver,Locators.AddToCartPage.Button.updateAll );
			if(!bStatus) {
				Messages.errorMsg = "UpdateAll Button not clicked";
				return false;
			}


			bStatus = Wait.waitForElementVisibility(Global.wDriver,Locators.AddToCartPage.TextField.estimatedArrival, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Estimated Arrival TextField not visible";
				return false;
			}

			String[] estimateArr=Elements.getText(Global.wDriver, Locators.AddToCartPage.TextField.estimatedArrival).split(":");
			sEstimatedArrival=estimateArr[2].trim();

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.CartPage.TextBox.enterPOnumber, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "PO number field not visible";
				return false;
			}

			bStatus = Elements.enterText(Global.wDriver, Locators.CartPage.TextBox.enterPOnumber, details.get("PO Number"));
			if(!bStatus) {
				Messages.errorMsg = "PO number entered successfully";
				return false;
			}

			


			bStatus = Wait.waitForElementVisibility(Global.wDriver,Locators.AddToCartPage.Button.processToCheckOut, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Process To CheckOut Button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver,Locators.AddToCartPage.Button.processToCheckOut );
			if(!bStatus) {
				Messages.errorMsg = "Process To CheckOut Button not clicked";
				return false;
			}
			
			
			return true;

		}
		catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
	}

	public static boolean verifyCheckoutPage(Map<String, String> details) {

		try{

			float fExpWeight = 0;
			float fExpSubTotal = 0;

			for(String productId : productIdList) {

				Map<String,String> productDetails = new HashMap<String, String>();
				productDetails = AllProductDetails.get(productId);

				String sQuantity = AllProductDetails.get(productId).get("quantity");
				int iQuantity = Integer.parseInt(sQuantity);

				String sProductPrice = AllProductDetails.get(productId).get("yourPrice");
				float fPrice = Float.parseFloat(sProductPrice);
				fExpSubTotal += fPrice*iQuantity; 

				bStatus=Wait.waitForElementVisibility(Global.wDriver, Locators.CheckoutPage.TextField.pageTitle, Constants.waitForPage);
				if(!bStatus) {
					Messages.errorMsg = "Checkout page not displayed";
					return false;
				}

				String sWeight = AllProductDetails.get(productId).get("weight");
				float fWeight = Float.parseFloat(sWeight);
				fExpWeight += fWeight*iQuantity;

			}

			String sSubTotal = Elements.getText(Global.wDriver, Locators.CartPage.TextField.totalSubtotal);
			sSubTotal = sSubTotal.trim().substring(1);
			if(sSubTotal.length()>=8) {
				sSubTotal = sSubTotal.replace(",", "");
			}
			float fActualSubTotal = Float.parseFloat(sSubTotal);

			Assert.assertEquals(fActualSubTotal, fExpSubTotal);

			String sWeight = Elements.getText(Global.wDriver, Locators.CartPage.TextField.totalWeight);
			sWeight = sWeight.trim().substring(0, sWeight.indexOf(" "));
			float fActualWeight = Float.parseFloat(sWeight);

			Assert.assertEquals(fActualWeight, fExpWeight);

			String sEstimatedShipping = Elements.getText(Global.wDriver, Locators.CartPage.TextField.estimatedShipping);
			sEstimatedShipping = sEstimatedShipping.trim().substring(1);
			if(sEstimatedShipping.length()>=8) {
				sEstimatedShipping = sEstimatedShipping.replace(",", "");
			}
			float fEstimatedShipping = Float.parseFloat(sEstimatedShipping);

			String sEstimatedTaxes = Elements.getText(Global.wDriver, Locators.CartPage.TextField.estimatedTaxes);
			sEstimatedTaxes = sEstimatedTaxes.trim().substring(1);
			if(sEstimatedTaxes.length()>=8) {
				sEstimatedTaxes = sEstimatedTaxes.replace(",", "");
			}
			float fEstimatedTaxes = Float.parseFloat(sEstimatedTaxes);

			float fExpEstimatedTotal = fExpSubTotal + fEstimatedShipping + fEstimatedTaxes;

			String sEstimatedTotal = Elements.getText(Global.wDriver, Locators.CartPage.TextField.estimatedTotal);
			sEstimatedTotal = sEstimatedTotal.trim().substring(1);
			if(sEstimatedTotal.length()>=8) {
				sEstimatedTotal = sEstimatedTotal.replace(",", "");
			}
			float fActualEstimatedTotal = Float.parseFloat(sEstimatedTotal);

			Assert.assertEquals(fActualEstimatedTotal, fExpEstimatedTotal);

			String paymentType=Locators.AddToCartPage.RadioButton.paymentType.replace("PaymentType", details.get("Payment Type"));
			bStatus = Wait.waitForElementVisibility(Global.wDriver,By.xpath(paymentType), Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Payment Type Radio Button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver,By.xpath(paymentType));
			if(!bStatus) {
				Messages.errorMsg = "Payment Type not clicked";
				return false;
			}
			ApplicationFunctions.clickElementUsingJs(By.xpath(Locators.AddToCartPage.Dropdown.newCardType));
//			bStatus = Wait.waitForElementVisibility(Global.wDriver,By.xpath(Locators.AddToCartPage.Dropdown.newCardType), Constants.waitForElement);
//			if(!bStatus) {
//				Messages.errorMsg = "New Card Type Dropdown not visible";
//				return false;
//			}

			bStatus = Elements.selectOptionByVisibleText(Global.wDriver,Locators.AddToCartPage.Dropdown.newCardType,"New Credit Card");
			if(!bStatus) {
				Messages.errorMsg = "New Card Type not selected";
				return false;
			}
Thread.sleep(3000);
			//ApplicationFunctions.clickElementUsingJs(By.xpath(Locators.AddToCartPage.Dropdown.cardType));
//			bStatus = Wait.waitForElementVisibility(Global.wDriver,By.xpath(Locators.AddToCartPage.Dropdown.cardType), Constants.waitForElement);
//			if(!bStatus) {
//				Messages.errorMsg = "Card Type Dropdown not visible";
//				return false;
//			}
             Global.wDriver.switchTo().frame("dieCommFrame");
             ApplicationFunctions.clickElementUsingJs(By.xpath(Locators.AddToCartPage.Dropdown.cardType));
			bStatus = Elements.selectOptionByVisibleText(Global.wDriver,Locators.AddToCartPage.Dropdown.cardType,details.get("CardType").trim());
			if(!bStatus) {
				Messages.errorMsg = "Card Type not selected";
				return false;
			}


			bStatus = Wait.waitForElementVisibility(Global.wDriver,Locators.AddToCartPage.TextBox.cardNumber, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Card Number not visible";
				return false;
			}

			bStatus = Elements.enterText(Global.wDriver,Locators.AddToCartPage.TextBox.cardNumber,details.get("CardNumber"));
			if(!bStatus) {
				Messages.errorMsg = "Card Number not entered";
				return false;
			}
			bStatus = Wait.waitForElementVisibility(Global.wDriver,Locators.AddToCartPage.TextBox.cardHolderName, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Card HolderName Field not visible";
				return false;
			}

			bStatus = Elements.enterText(Global.wDriver,Locators.AddToCartPage.TextBox.cardHolderName,details.get("Name On Card"));
			if(!bStatus) {
				Messages.errorMsg = "Card HolderName not entered";
				return false;
			}
			ApplicationFunctions.clickElementUsingJs(By.xpath(Locators.AddToCartPage.Dropdown.cardExpMonth));
//			bStatus = Wait.waitForElementVisibility(Global.wDriver,By.xpath(Locators.AddToCartPage.Dropdown.cardExpMonth), Constants.waitForElement);
//			if(!bStatus) {
//				Messages.errorMsg = "Card Expire Month Dropdown not visible";
//				return false;
//			}

			bStatus = Elements.selectOptionByVisibleText(Global.wDriver,Locators.AddToCartPage.Dropdown.cardExpMonth,details.get("ExpiryMonth"));
			if(!bStatus) {
				Messages.errorMsg = "Card Expire Month not selected";
				return false;
			}
			ApplicationFunctions.clickElementUsingJs(By.xpath(Locators.AddToCartPage.Dropdown.cardExpYear));

//			bStatus = Wait.waitForElementVisibility(Global.wDriver,By.xpath(Locators.AddToCartPage.Dropdown.cardExpYear), Constants.waitForElement);
//			if(!bStatus) {
//				Messages.errorMsg = "Card Expire Year Dropdown not visible";
//				return false;
//			}

			bStatus = Elements.selectOptionByVisibleText(Global.wDriver,Locators.AddToCartPage.Dropdown.cardExpYear,"20"+details.get("ExpiryYear"));
			if(!bStatus) {
				Messages.errorMsg = "Card Expire Year not selected";
				return false;
			}
			Global.wDriver.switchTo().defaultContent();
			
			Thread.sleep(2000);
			
			
			bStatus = Wait.waitForElementVisibility(Global.wDriver,Locators.AddToCartPage.Button.reviewOrder, Constants.waitForElement);
			if(!bStatus) {
				Messages.errorMsg = "Review Order Button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver,Locators.AddToCartPage.Button.reviewOrder);
			if(!bStatus) {
				Messages.errorMsg = "Review Order not clicked";
				return false;
			}
//			bStatus = Elements.clickButton(Global.wDriver, Locators.CartPage.Button.checkout);
//			if(!bStatus) {
//				Messages.errorMsg = "Checkbox button not clicked";
//				return false;
//			}

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.CheckoutPage.Button.submitOrder, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Submit Order button not clicked";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.CheckoutPage.Button.submitOrder);
			if(!bStatus) {
				Messages.errorMsg = "Submit Order button not clicked";
				return false;
			}

			return true;

		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static Map<String, Object> verifyOrderSummaryPage() {

		Map<String,Object> result = new HashMap<String, Object>();

		try {

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.OrderSummaryPage.Button.orderSummary, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Order Summary page is not visible";
				result.put("status", false);
				return result;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.OrderSummaryPage.Button.orderSummary);
			if(!bStatus) {
				Messages.errorMsg = "Order Summary button not clicked";
				result.put("status", false);
				return result;
			}

			float fExpSubTotal = 0;
			float fExpWeight = 0;

			for(String productId : productIdList) {

				String productUnitPriceLocator = Locators.OrderSummaryPage.TextField.productUnitPrice.replace("productId", productId);
				String sUnitPrice = Elements.getText(Global.wDriver, By.xpath(productUnitPriceLocator)).trim().substring(1);
				if(sUnitPrice.length()>=8) {
					sUnitPrice = sUnitPrice.replace(",", "");
				}
				float fProductPrice = Float.parseFloat(sUnitPrice);
				int quantity = Integer.parseInt(AllProductDetails.get(productId).get("quantity"));

				fExpSubTotal += fProductPrice*quantity;

				String poNumberLocator = Locators.OrderSummaryPage.TextField.productPONumber.replace("productId", productId);
				String sPONumber = Elements.getText(Global.wDriver, By.xpath(poNumberLocator)).trim();

				//				Assert.assertEquals(sPONumber, details.get("PO Number"));

				float productWeight = Float.parseFloat(AllProductDetails.get(productId).get("weight"));
				fExpWeight += productWeight*quantity;

			}

			String sSubTotal = Elements.getText(Global.wDriver, Locators.OrderSummaryPage.TextField.totalSubtotal);
			sSubTotal = sSubTotal.trim().substring(1);
			if(sSubTotal.length()>=8) {
				sSubTotal = sSubTotal.replace(",", "");
			}
			float fActualSubTotal = Float.parseFloat(sSubTotal);

			Assert.assertEquals(fActualSubTotal, fExpSubTotal);

			String sWeight = Elements.getText(Global.wDriver, Locators.OrderSummaryPage.TextField.totalWeight);
			sWeight = sWeight.trim().substring(0, sWeight.indexOf(" "));
			float fActualWeight = Float.parseFloat(sWeight);

			Assert.assertEquals(fActualWeight, fExpWeight);

			String sEstimatedShipping = Elements.getText(Global.wDriver, Locators.OrderSummaryPage.TextField.estimatedShipping);
			sEstimatedShipping = sEstimatedShipping.trim().substring(1);
			if(sEstimatedShipping.length()>=8) {
				sEstimatedShipping = sEstimatedShipping.replace(",", "");
			}
			float fEstimatedShipping = Float.parseFloat(sEstimatedShipping);

			String sEstimatedTaxes = Elements.getText(Global.wDriver, Locators.OrderSummaryPage.TextField.estimatedTaxes);
			sEstimatedTaxes = sEstimatedTaxes.trim().substring(1);
			if(sEstimatedTaxes.length()>=8) {
				sEstimatedTaxes = sEstimatedTaxes.replace(",", "");
			}
			float fEstimatedTaxes = Float.parseFloat(sEstimatedTaxes);

			float fExpEstimatedTotal = fExpSubTotal + fEstimatedShipping + fEstimatedTaxes;

			String sEstimatedTotal = Elements.getText(Global.wDriver, Locators.OrderSummaryPage.TextField.estimatedTotal);
			sEstimatedTotal = sEstimatedTotal.trim().substring(1);
			if(sEstimatedTotal.length()>=8) {
				sEstimatedTotal = sEstimatedTotal.replace(",", "");
			}
			float fActualEstimatedTotal = Float.parseFloat(sEstimatedTotal);

			Assert.assertEquals(fActualEstimatedTotal, fExpEstimatedTotal);

			String orderNumber = Elements.getText(Global.wDriver, Locators.OrderSummaryPage.TextField.orderNumber);

			orderNumber = orderNumber.substring(10, 20);

			sOrderNumber = orderNumber;
			System.out.println(sOrderNumber);

			result.put("orderNumber", orderNumber);
			result.put("status", true);
			return result;

		} catch(Exception e) {
			e.printStackTrace();
			result.put("status", false);
			return result;
		}

	}

	public static boolean logout() {

		try{

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Button.logo, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Home button not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Button.logo);
			if(!bStatus) {
				Messages.errorMsg = "Home button not clicked";
				return false;
			}

			bStatus = Wait.waitForElementVisibility(Global.wDriver, Locators.HomePage.Link.signOut, Constants.waitForPage);
			if(!bStatus) {
				Messages.errorMsg = "Logout link not visible";
				return false;
			}

			bStatus = Elements.clickButton(Global.wDriver, Locators.HomePage.Link.signOut);
			if(!bStatus) {
				Messages.errorMsg = "Logout link not clicked";
				return false;
			}

			return true;

		}catch(Exception e) {

			e.printStackTrace();
			return false;

		}

	}

	public static  boolean addProductsToCart(String sCatalogType, String sFilePath){
		try{
			int iRowCount= GeneralFunctions.getExcelRowCount(sFilePath, sCatalogType);

			for(int currentRow=1 ; currentRow <iRowCount;currentRow++){
				Map<String , String> productCatalogDetails= Utilities.readTestData(sFilePath, sCatalogType, currentRow+"");
				bStatus=navigateToHomePage();
				if(!bStatus){
					Messages.errorMsg = Messages.errorMsg+"Unable to Navigate to Home Page";
					return false;

				}
				bStatus=selectProductCatalogType(productCatalogDetails);
				if(!bStatus){
					Messages.errorMsg = Messages.errorMsg+"Unable to Select the CatalogType";
					return false;

				}
				bStatus=selectSubCategory(productCatalogDetails);
				if(!bStatus){
					Messages.errorMsg = Messages.errorMsg+"Unable to select the SubCategory";
					return false;

				}
				//				bStatus=selectAProduct(productCatalogDetails.get("ProductId"),productCatalogDetails.get("Quantity"));
				if(!bStatus){
					Messages.errorMsg = Messages.errorMsg+"Unable to select a Product";
					return false;

				}

			}
		}catch(Exception ex){
			ex.printStackTrace();
			return false;
		}
		return true;
	}
	public static Map<String, String>  productAvailability()
	{
		Map<String,String> productAvailabilityDetails=new LinkedHashMap<String, String>();
		try
		{
			String errorMsg="Product is not available";	
			String shipto=Elements.getText(Global.wDriver, Locators.CatalogPage.TextField.shipTo);
			String pickup=Elements.getText(Global.wDriver,Locators.CatalogPage.TextField.pickUpStatus);
			productAvailabilityDetails.put("ShiptoStatus", shipto);
			productAvailabilityDetails.put("PickupStatus", pickup);

			if(shipto.contains("item is not available"))
				////		{
				//////			 String shipToEstimatedArrival=Elements.getText(Global.wDriver,Locators.CatalogPage.TextField.shipToEstimatedArrival);
				//////			 productAvailabilityDetails.put("EstimatedArrival", shipToEstimatedArrival);
				//////			
				////		}
				//		else
			{
				productAvailabilityDetails.put("ShipToErrorMsg", errorMsg);
			}
			if(pickup.contains("item is not available")){
				productAvailabilityDetails.put("PickUpProductErrorMsg", errorMsg);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();

		}

		return productAvailabilityDetails;
	}

	public static Map<String,String> getProductPrice()
	{
		Map<String,String> productPricedetails=new LinkedHashMap<String, String>();
		try
		{
			String yourPrice=Elements.getText(Global.wDriver,Locators.CatalogPage.TextField.yourProductPrices);
			String[] yoursprice=yourPrice.split(":");
			String listPrice=Elements.getText(Global.wDriver,Locators.CatalogPage.TextField.listProductPrices).replace(yourPrice, "");
			String[] listsprice=listPrice.split(":");
			productPricedetails.put("YourPrice",yoursprice[1].replace("$", ""));
			productPricedetails.put("ListPrice",listsprice[1].replace("$", ""));
			System.out.println(yourPrice);
			System.out.println(listPrice);


		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return productPricedetails;
	}

	public static void addCartItemsToResultXls(String fileName, String sheetName, Map<String, String> details) {

		try{

			Workbook wb = Workbook.getWorkbook(new File(fileName));
			WritableWorkbook wbCopy = Workbook.createWorkbook(new File(fileName), wb);
			WritableSheet s = wbCopy.getSheet(sheetName);
			WritableSheet s1 = wbCopy.getSheet("Result");
			
			int productCount = productIdList.size();
			
			for(int iRow = s1.getRows(); iRow<productCount; iRow++) {
				
				Label orderNumberLbl = new Label(5, iRow, sOrderNumber);
				s1.addCell(orderNumberLbl);
				
			}
			
			Label orderNumberLbl = new Label(9, 0, sOrderNumber);
			s.addCell(orderNumberLbl);

			Label shippingMethodLbl = new Label(12, 0, details.get("Shipping Method"));
			s.addCell(shippingMethodLbl);

			Label estimatedArrivalLbl = new Label(12, 2, sEstimatedArrival);
			s.addCell(estimatedArrivalLbl);

			float estimatedTotal = 0;

			int rowCount = 3;
			for(String productId : productIdList) {

				Label productIdLbl = new Label(7, rowCount, productId);
				s.addCell(productIdLbl);

				float yourPrice = Float.parseFloat(AllProductDetails.get(productId).get("yourPrice"));
				Label yourPriceLbl = new Label(8, rowCount, String.valueOf(yourPrice));
				s.addCell(yourPriceLbl);

				int quantity = Integer.parseInt(AllProductDetails.get(productId).get("quantity"));
				Label quantityLbl = new Label(9, rowCount, String.valueOf(quantity));
				s.addCell(quantityLbl);

				float prodTotalPrice = yourPrice * quantity;
				Label prodTotalPriceLbl = new Label(10, rowCount, String.valueOf(prodTotalPrice));
				s.addCell(prodTotalPriceLbl);

				estimatedTotal += prodTotalPrice;

				rowCount++;
			}

			Label estimatedTotalLbl = new Label(11, 2, String.valueOf(estimatedTotal));
			s.addCell(estimatedTotalLbl);

			wbCopy.write();
			wbCopy.close();
			wb.close();

		}catch(Exception e) {
			e.printStackTrace();
		}

	}

	public static void clearResultsFromXls(String fileName, String sheetName) {

		try{

			Workbook wb = Workbook.getWorkbook(new File(fileName));

			WritableWorkbook wbCopy = Workbook.createWorkbook(new File(fileName), wb);
			WritableSheet s = wbCopy.getSheet(sheetName);

			for(int rowCount = 3; rowCount<=50; rowCount++) {

				for(int colCount = 0; colCount <=10; colCount++) {

					Label lbl = new Label(colCount, rowCount, "");
					s.addCell(lbl);

				}

			}

			wbCopy.write();
			wbCopy.close();
			wb.close();

		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	public static void addProductDetailsToResultXls(String fileName, String sheetName, String productId,
			String quantity, Map<String, String> details) {

		try{

			Workbook wb = Workbook.getWorkbook(new File(fileName));
			WritableWorkbook wbCopy = Workbook.createWorkbook(new File(fileName), wb);
			WritableSheet s = wbCopy.getSheet(sheetName);

			int rowCount = 2 + productIdList.size();

			String userName = details.get("UserName");
			Label usernameLbl = new Label(2, 1, userName);
			s.addCell(usernameLbl);


			String catalogType = details.get("Product Catalog");
			Label catalogTypeLbl = new Label(0, rowCount, catalogType);
			s.addCell(catalogTypeLbl);

			String menuPath = details.get("Menu Path");
			Label menuPathLbl = new Label(1, rowCount, menuPath);
			s.addCell(menuPathLbl);

			Label productIdLbl = new Label(2, rowCount, productId);
			s.addCell(productIdLbl);

			String listPrice = AllProductDetails.get(productId).get("listPrice");
			Label listPriceLbl = new Label(3, rowCount, listPrice);
			s.addCell(listPriceLbl);

			String yourPrice = AllProductDetails.get(productId).get("yourPrice");
			Label yourPriceLbl = new Label(4, rowCount, yourPrice);
			s.addCell(yourPriceLbl);

			Label quantityLbl = new Label(5, rowCount, quantity);
			s.addCell(quantityLbl);

			wbCopy.write();
			wbCopy.close();
			wb.close();


		} catch(Exception e) {

			e.printStackTrace();

		}

	}

	private static void addQuickOrderProductDetailsToResultXls(String filePath,
			String sheetName, String productId, String quantity, int index) {

		try{

			Workbook wb = Workbook.getWorkbook(new File(filePath));
			WritableWorkbook wbCopy = Workbook.createWorkbook(new File(filePath), wb);
			WritableSheet s = wbCopy.getSheet(sheetName);

			int rowCount = 2 + productIdList.size() - quickOrderProductIDList.size() + index;

			Label catalogTypeLbl = new Label(0, rowCount, "QuickOrder");
			s.addCell(catalogTypeLbl);

			Label menuPathLbl = new Label(1, rowCount, "NA");
			s.addCell(menuPathLbl);

			Label productIdLbl = new Label(2, rowCount, productId);
			s.addCell(productIdLbl);

			String listPrice = AllProductDetails.get(productId).get("listPrice");
			Label listPriceLbl = new Label(3, rowCount, listPrice);
			s.addCell(listPriceLbl);

			String yourPrice = AllProductDetails.get(productId).get("yourPrice");
			Label yourPriceLbl = new Label(4, rowCount, yourPrice);
			s.addCell(yourPriceLbl);

			Label quantityLbl = new Label(5, rowCount, quantity);
			s.addCell(quantityLbl);

			wbCopy.write();
			wbCopy.close();
			wb.close();


		} catch(Exception e) {

			e.printStackTrace();

		}

	}
public static void clickElementUsingJs(By locater){
WebElement element=Global.wDriver.findElement(locater);
	JavascriptExecutor js = (JavascriptExecutor)Global.wDriver;
	js.executeScript("arguments[0].click();", element);
}
}