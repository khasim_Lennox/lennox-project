package com.tenx.lennox.testcases;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.*;

import com.tenx.framework.lib.Browser;
import com.tenx.framework.lib.Messages;
import com.tenx.framework.lib.Utilities;
import com.tenx.framework.reporting.Reporting;
import com.tenx.lennox.lib.ApplicationFunctions;
import com.tenx.lennox.variables.Global;

public class PLP_Residential {
	public static boolean bStatus;
	public static String sTestCaseName;


	@BeforeTest
	public void setup(){
		Reporting.Functionality = "Lennox";
		try{
			Global.wDriver = Browser.openBrowser(Global.sBrowserName, Global.sURL, Global.sPathOfChromeDriver);
			if(Global.wDriver == null){
				Reporting.logResults("Fail", "Open Web Application", "Application not opened.");
				Assert.fail("Application not opened.");
			} 
			Reporting.logResults("Pass", "Open Web Application", "Application opened successfully"); 

			bStatus = ApplicationFunctions.loginToApplication(Global.sUserId,Global.sPassword);
			if(!bStatus) {
				Reporting.logResults("Fail", "Login to Application", "Login to application failed."+Messages.errorMsg);
				Assert.fail("Login to application failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Login to Application", "Logged in to application successfully");

			bStatus = ApplicationFunctions.clearCart();
			if(!bStatus) {
				Reporting.logResults("Fail", "Clear cart", "Clearing cart failed."+Messages.errorMsg);
				Assert.fail("Clearing cart failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Clear cart", "Cart cleared successfully");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	@Test
	public void product_Residential(){
		try{
			Map<String,String> Details = Utilities.readTestData("TestData\\LennoxTestData.xls", "TestData", "TC01");
			Map<String, Map<String,String>> proDetails = Utilities.readMultipleTestData("TestData\\LennoxTestData.xls", Details.get("Product Catalog"), Details.get("Product Catalog"));		
			int iValue = proDetails.size();
			System.out.println(iValue);
			for(int iRowCounter = 0; iRowCounter<iValue;iRowCounter++)	
			{
				Map<String , String>objDetails = proDetails.get("Row" + (iRowCounter + 1));
				// get the row wise data from test data file		
				String sCurentTCName = objDetails.get("TestScenarioID");

				bStatus = ApplicationFunctions.selectProductCatalogType(objDetails);
				if(!bStatus) {
					Reporting.logResults("Fail", "Select product catalog type", "Selecting product catalog type failed."+Messages.errorMsg);
					Assert.fail("Selecting product catalog type failed."+Messages.errorMsg);
				}
				Reporting.logResults("Pass", "Select product catalog type", "Product catalog type selected successfully");

				bStatus = ApplicationFunctions.selectSubCategory(objDetails);
				if(!bStatus) {
					Reporting.logResults("Fail", "Select product sub category", "Selecting product sub category failed."+Messages.errorMsg);
					Assert.fail("Selecting product sub category failed."+Messages.errorMsg);
				}
				Reporting.logResults("Pass", "Select product sub category", "Product sub category selected successfully");

				bStatus = ApplicationFunctions.selectAProduct(objDetails);
				if(!bStatus) {
					Reporting.logResults("Fail", "Select a product", "Selecting product with id "+objDetails.get("ProductId")+"failed."+Messages.errorMsg);
					Assert.fail("Selecting product product with id "+objDetails.get("ProductId")+" failed."+Messages.errorMsg);
				}
				Reporting.logResults("Pass", "Select a product", "Product with id "+objDetails.get("ProductId")+" selected and added to cart successfully");


				bStatus = ApplicationFunctions.readSpecification(objDetails, iRowCounter);
				if(!bStatus) {
					Reporting.logResults("Fail", "Read Product Specifications", "Unable to Read ProductDetails for  "+objDetails.get("ProductId")+"failed."+Messages.errorMsg);
					Assert.fail("Unable to Read ProductDetails for  "+objDetails.get("ProductId")+"failed."+Messages.errorMsg);
				}
				Reporting.logResults("Pass", "Read Product Specifications", "Read Specifications for  "+objDetails.get("ProductId")+" done  successfully");

				bStatus = ApplicationFunctions.navigateToHomePage();
				if(!bStatus) {
					Reporting.logResults("Fail", "Navigate To Homepage", "Navigation to home page failed."+Messages.errorMsg);
					Assert.fail("Navigation to home page failed."+Messages.errorMsg);
				}
				Reporting.logResults("Fail", "Navigate To Homepage", "Navigated to home page successfully");

			}

			bStatus = ApplicationFunctions.verifyCartMenu("Testdata\\LennoxTestData.xls", "Result");
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Cart Menu", "Validation of cart menu failed."+Messages.errorMsg);
				Assert.fail("Validation of cart menu failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Cart Menu", "Validation of cart menu successfull");

			bStatus = ApplicationFunctions.verifyCartPage(Details);
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Cart page", "Validation of cart page failed."+Messages.errorMsg);
				Assert.fail("Validation of cart page failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Cart page", "Validation of Cart page successfull");

			bStatus = ApplicationFunctions.verifyCheckoutPage(Details);
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Checkout page", "Validation of checkout page failed."+Messages.errorMsg);
				Assert.fail("Validation of checkout page failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Checkout page", "Validation of checkout page successfull");

			Map<String, Object> resultStatus = ApplicationFunctions.verifyOrderSummaryPage();
			bStatus = (boolean) resultStatus.get("status");
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Order Summary page", "Validation of Order Summary page failed."+Messages.errorMsg);
				Assert.fail("Validation of Order Summary page failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Order Summary page", "Validation of Order Summary page successfull and order number "+resultStatus.get("orderNumber")+" generated");

			bStatus = ApplicationFunctions.logout();
			if(!bStatus) {
				Reporting.logResults("Fail", "Logout from Application", "Logout from Application failed."+Messages.errorMsg);
				Assert.fail("Logout from Application failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Logout from Application", "Logged out from Application successfully");

			ApplicationFunctions.addCartItemsToResultXls("TestData\\LennoxTestData.xls", "BackUpResult", Details);
			
		}catch(Exception e){
			Messages.errorMsg=e.toString()+Messages.errorMsg;		
			e.printStackTrace();
		}

	}
//	@AfterTest
//	public void Teardown(){
//		Global.wDriver.quit();
//	}
}
