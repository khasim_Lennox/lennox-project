package com.tenx.lennox.testscripts;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tenx.framework.lib.Browser;
import com.tenx.framework.lib.Messages;
import com.tenx.framework.lib.Utilities;
import com.tenx.framework.reporting.Reporting;
import com.tenx.lennox.lib.ApplicationFunctions;
import com.tenx.lennox.variables.Global;

public class Lennox_TC01 {

	private static boolean bStatus;

	@BeforeMethod 
	public static void setup() {

		Reporting.Functionality = "Lennox";
		Reporting.Testcasename = "Add Products from Residential Catalog to Cart";
		
		try {
			
			Global.wDriver = Browser.openBrowser(Global.sBrowserName, Global.sURL, Global.sPathOfChromeDriver);
			if(Global.wDriver == null){
				Reporting.logResults("Fail", "Open Web Application", "Application not opened.");
				Assert.fail("Application not opened.");
			} 
			Reporting.logResults("Pass", "Open Web Application", "Application opened successfully"); 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test
	public static void addProductsToCart() {

		try{
			
			Map<String, String> Details = Utilities.readTestData("TestData\\LennoxTestData.xls", "TestData", "TC01");
			
			bStatus = ApplicationFunctions.loginToApplication(Global.sUserId,Global.sPassword);
			if(!bStatus) {
				Reporting.logResults("Fail", "Login to Application", "Login to application failed."+Messages.errorMsg);
				Assert.fail("Login to application failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Login to Application", "Logged in to application successfully");
			
			bStatus = ApplicationFunctions.clearCart();
			if(!bStatus) {
				Reporting.logResults("Fail", "Clear cart", "Clearing cart failed."+Messages.errorMsg);
				Assert.fail("Clearing cart failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Clear cart", "Cart cleared successfully");
			
			bStatus = ApplicationFunctions.selectProductCatalogType(Details);
			if(!bStatus) {
				Reporting.logResults("Fail", "Select product catalog type", "Selecting product catalog type failed."+Messages.errorMsg);
				Assert.fail("Selecting product catalog type failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Select product catalog type", "Product catalog type selected successfully");

			bStatus = ApplicationFunctions.selectSubCategory(Details);
			if(!bStatus) {
				Reporting.logResults("Fail", "Select product sub category", "Selecting product sub category failed."+Messages.errorMsg);
				Assert.fail("Selecting product sub category failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Select product sub category", "Product sub category selected successfully");
			
//			bStatus = ApplicationFunctions.selectAProduct(Details.get("Product Code 1"), Details.get("Quantity Code 1"));
			if(!bStatus) {
				Reporting.logResults("Fail", "Select a product", "Selecting product with id "+Details.get("Product Code 1")+"failed."+Messages.errorMsg);
				Assert.fail("Selecting product product with id "+Details.get("Product Code 1")+" failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Select a product", "Product with id "+Details.get("Product Code 1")+" selected and added to cart successfully");
			
//			bStatus = ApplicationFunctions.selectAProduct(Details.get("Product Code 2"), Details.get("Quantity Code 2"));
			if(!bStatus) {
				Reporting.logResults("Fail", "Select a product", "Selecting product with id "+Details.get("Product Code 2")+"failed."+Messages.errorMsg);
				Assert.fail("Selecting product product with id "+Details.get("Product Code 2")+" failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Select a product", "Product with id "+Details.get("Product Code 2")+" selected and added to cart successfully");
			
			
			bStatus = ApplicationFunctions.verifyCartMenu(null, null);
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Cart Menu", "Validation of cart menu failed."+Messages.errorMsg);
				Assert.fail("Validation of cart menu failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Cart Menu", "Validation of cart menu successfull");
			
			bStatus = ApplicationFunctions.verifyCartPage(Details);
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Cart page", "Validation of cart page failed."+Messages.errorMsg);
				Assert.fail("Validation of cart page failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Cart page", "Validation of Cart page successfull");
			
			bStatus = ApplicationFunctions.verifyCheckoutPage(Details);
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Checkout page", "Validation of checkout page failed."+Messages.errorMsg);
				Assert.fail("Validation of checkout page failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Checkout page", "Validation of checkout page successfull");
			
			Map<String, Object> resultStatus = ApplicationFunctions.verifyOrderSummaryPage();
			bStatus = (boolean) resultStatus.get("status");
			if(!bStatus) {
				Reporting.logResults("Fail", "Validate Order Summary page", "Validation of Order Summary page failed."+Messages.errorMsg);
				Assert.fail("Validation of Order Summary page failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Validate Order Summary page", "Validation of Order Summary page successfull and order number "+resultStatus.get("orderNumber")+" generated");
			
			bStatus = ApplicationFunctions.logout();
			if(!bStatus) {
				Reporting.logResults("Fail", "Logout from Application", "Logout from Application failed."+Messages.errorMsg);
				Assert.fail("Logout from Application failed."+Messages.errorMsg);
			}
			Reporting.logResults("Pass", "Logout from Application", "Logged out from Application successfully");
			
		}catch(Exception e) {
			e.printStackTrace();
		}

	}

	@AfterClass
	public static void tearDown(){

		Global.wDriver.quit();

	}	
}
