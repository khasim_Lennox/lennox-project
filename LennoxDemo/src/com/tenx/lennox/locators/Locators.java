package com.tenx.lennox.locators;

import org.openqa.selenium.By;

public class Locators {

	public static class HomePage {

		public static class Link {

//			public static By login = By.linkText("Login");
//
//			public static By logout = By.linkText("Logout");
			public static String productLink = "//li[text()='Product Catalog']/following-sibling::li/a[text()='prodCatType']";
			public static By viewItemsInCart = By.xpath("//a[text()='View All Items']");

			public static By signIn=By.xpath("//ul[@class='links pull-right']/li/a[contains(text(),' Sign In')]");

			public static By signOut=By.xpath("//ul[@class='links pull-right']/li/a[contains(text(),'Sign Out')]");


		}

		public static class Button {

			public static By logo = By.xpath("//div[@class='logo']/a");
			public static By memberSignIn=By.xpath("//div/a[text()='SIGN IN']");
			public static By viewCart = By.xpath("//a[contains(.,'Cart')]");
			public static By quickOrder = By.xpath("//a[contains(.,'Quick Order')]");
			public static By addToCartByQuickOrder = By.xpath("//form[@id='quick-order-form']//button[text()='ADD TO CART']");
			public static By checkout= By.xpath("//a[text()='CHECKOUT NOW']");


		}

		public static class TextBox {

			public static By catalogNumberForQuickOrder = By.xpath("//li[2]//input[@name='sku']");
			public static By quantityForQuickOrder = By.xpath("//li[2]//input[@name='qty']");

		}

		public static class TextField {
			//li[@class='welcome-msg']
			public static By welcomeMessage = By.xpath("//div[text()='Welcome,']");			
			public static String productPrice = "//li[@class='product productId']//div[@class='product-amount']";
			public static By subTotal = By.xpath("//h5[text()='Subtotal:']/following-sibling::h5");

		}

	}

	public static class LoginPage {

		public static class TextBox {

			public static By userId = By.id("j_username");
			public static By password = By.id("j_password");

		}

		public static class Button {

			public static By signIn = By.xpath("//button[text()='Sign In']");

		}

	}

	public static class CatalogPage {

		public static class Link {

			public static String category = "//h4[text()='Shop by']/parent::li//ul//a[text()='subCategory']";

			public static By shipTo=By.xpath("//label[contains(text(),'Ship to')]/following-sibling::div/strong");


		}

		public static class TextBox {

			public static By quantity = By.xpath("//input[@id='qty-input']");

		}

		public static class TextField {

			public static String productId = "//div[@class='sku'][contains(.,'productId')]";
			public static By description=By.xpath("//span[@itemprop='mpn']");
			public static String yourPrice = "//div[@class='sku'][contains(.,'productId')]/following-sibling::div//p[@class='your-price']";
			public static String listPrice = "//div[@class='sku'][contains(.,'productId')]/following-sibling::div//p[@class='price']";
			public static By shipTo=By.xpath("//label[contains(text(),'Ship to')]/following-sibling::div/strong");
			public static By shipToEstimatedArrival=By.xpath("//label[contains(text(),'Ship to')]/following-sibling::div[contains(.,'Estimated Arrival:')]/b");
			public static By pickUpStatus=By.xpath("//label[contains(text(),'Pick up')]/following-sibling::div/strong");

			public static By yourProductPrices=By.xpath(" //p[@class='price']/strong[contains(.,'Your price')]");
			public static By listProductPrices=By.xpath(" //p[@class='price'][contains(.,'List price')]");




		}

		public static class Button {
			public static By specifications=By.xpath("//li/a[text()='Specifications']");  
			public static String addToCart = "//button[@id='product-button-productId']";
			public static By continueShopping = By.xpath("//div[@id='add-to-cart-success']//a[text()='close']");

		}

	}

	public static class CartPage {

		public static class Link {

			public static By clearCart = By.xpath("//a[text()='Clear Cart']");

		}
		public static class TextField{

			public static String productWeight = "//ul[@class='item-options']/li[contains(.,'productId')]/following-sibling::li";
			public static By totalWeight = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Weight')]/following-sibling::td");
			public static By totalSubtotal = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Subtotal')]/following-sibling::td");
			public static By estimatedShipping = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Shipping')]/following-sibling::td");
			public static By estimatedTaxes = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Taxes')]/following-sibling::td");
			public static By estimatedTotal = By.xpath("//table[@id='orderTotals']//td/strong[text()='Estimated Total']/parent::td/following-sibling::td/strong");
			public static String totalPrice = "//tr[@data-product-id='productId']/td[@class='total last']/span";


		}
		public static class TextBox { 

			public static By enterPOnumber = By.xpath("//input[@id='checkout-po']");

		}

		public static class Button {

			public static By checkout = By.xpath("//a[contains(.,'Proceed to Checkout')]");
			public static By areYouSureYesButton=By.xpath("//div[@class='button-set']/a[text()='Yes']");

		}

	}

	public static class CheckoutPage {

		public static class RadioButton {

			public static String selectPaymentType = "//label[contains(.,'paymentType')]";

		}

		public static class Button {

			public static By reviewOrder = By.xpath("//button[text()='REVIEW ORDER']");
			public static By submitOrder = By.xpath("//button[contains(.,'Submit Order')]");

		}

		public static class TextField{

			public static By pageTitle = By.xpath("//h1[text()='Checkout']");
			public static By totalWeight = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Weight')]/following-sibling::td");
			public static By totalSubtotal = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Subtotal')]/following-sibling::td");
			public static By estimatedShipping = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Shipping')]/following-sibling::td");
			public static By estimatedTaxes = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Taxes')]/following-sibling::td");
			public static By estimatedTotal = By.xpath("//table[@id='orderTotals']//td/strong[text()='Estimated Total']/parent::td/following-sibling::td/strong");
			public static String totalPrice = "//tr[@data-product-id='productId']/td[@class='total last']/span";

		}

	}

	public static class OrderSummaryPage{

		public static class Button {

			public static By orderSummary = By.xpath("//div[@id='order-summary-accordion']");

		}

		public static class TextField {

			public static By orderNumber = By.xpath("//p[contains(.,'Order #')]");
			public static String productUnitPrice = "//tr[contains(.,'productId')]/td[@data-header='Unit Price']";
			public static String productTotalPrice = "//tr[contains(.,'productId')]/td[@data-header='Extended Price']";
			public static String productQuantity = "//tr[contains(.,'productId')]/td[@data-header='Qty']";
			public static String productPONumber = "//tr[contains(.,'productId')]/td[@data-header='PO #']";
			public static By totalWeight = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Weight')]/following-sibling::td");
			public static By totalSubtotal = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Subtotal')]/following-sibling::td");
			public static By estimatedShipping = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Shipping')]/following-sibling::td");
			public static By estimatedTaxes = By.xpath("//table[@id='orderTotals']//td[contains(text(),'Taxes')]/following-sibling::td");
			public static By estimatedTotal = By.xpath("//table[@id='orderTotals']//td/strong[text()='Total']/parent::td/following-sibling::td/strong");

		}

	}
	public static class AddToCartPage{
		public static class Link{
		public static By updateShopping=By.xpath("//li/a[contains(text(),'Update Shipping/Pick Up for all Line Items')]");
		}
		public static class Dropdown{
		public static String shippingMethod="//select[@id='shippingMethod']";
		public static String newCardType="//div/select[@id='ccard']";
		public static String cardType="//div/select[@id='c-ct']";
		public static String cardExpMonth="//div/select[@id='c-exmth']";
		public static String cardExpYear="//div/select[@id='c-exyr']";
		
	}
		public static class Button{
			public static By updateAll=By.xpath("//li/button[text()='Update All Items']");
			public static By processToCheckOut=By.xpath("//div/a[contains(.,'Proceed to Checkout')]");
			//public static By reviewOrder=By.xpath("//div/button[contains(.,'REVIEW ORDER')]");
			public static By reviewOrder=By.xpath("//div[@class='sidebar-btn-block']/div");
			
		}
		  
		  public static class TextField
		  {
		   public static By estimatedArrival=By.xpath("//div[@class='brand-primary']");
		  }
		  public static class RadioButton{
			  public static String paymentType="//li/div/label[contains(.,'PaymentType')]";
		  }
		  
		  public static class TextBox{
			  public static By cardNumber=By.xpath("//div/input[@id='c-cn']");
			  public static By cardHolderName=By.xpath("//div/input[@id='c-chn']");
		  }
		   
	}
}
