package com.tenx.lennox.testsuite;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import jxl.Sheet;
import jxl.Workbook;

import com.tenx.framework.lib.Messages;

public class Testsuite {
	public static void main(String[] args) throws Exception {

		XmlSuite suite = new XmlSuite();
		suite.setName("TestSuite");
		XmlTest test = new XmlTest(suite);
		test.setName("Lennox test");
		List<XmlClass> classes = new ArrayList<XmlClass>();
		for (int i = 0; i < getTestcaseList("TestData//LennoxTestData.xls").length; i++) {
			String sTCName =  "com.tenx.lennox.testscripts."+getTestcaseList("TestData//LennoxTestData.xls")[i];
			classes.add(new XmlClass(sTCName));
		}
		test.setXmlClasses(classes); 
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		TestNG tng = new TestNG();
		tng.setXmlSuites(suites);
		tng.run();
		//Script.
	}


	public static String[] getTestcaseList(String sFilePath)
	{
		String[] sTestCaseList = null;

		try {

			Workbook objWorkbook = Workbook.getWorkbook(new File(sFilePath));
			Sheet objSheet = objWorkbook.getSheet("TestCaseList");
			int iRowCount = objSheet.getRows();
			sTestCaseList = new String[iRowCount-1];
			int iColCount = objSheet.getColumns();
			for (int iRowCounter = 1; iRowCounter < iRowCount; iRowCounter++) {
				for (int iColCounter = 0; iColCounter < iColCount; iColCounter++) {
					sTestCaseList[iRowCounter-1] = objSheet.getCell(iColCounter, iRowCounter).getContents();
				}
			}

		}
		catch(Exception e)
		{
			Messages.errorMsg = e.getMessage();
			e.printStackTrace();
		}

		return sTestCaseList;
	}

	/*	public static void main(String[] args) {
		System.out.println(getTestcaseList("TestData//Data.xls")[0]);
	}*/

}
